﻿using System;

namespace ConsoleApp56
{
    class Program
    {
        static void Main(string[] args)
        {



            int[] arr = { 50, 10, 25, 54, 500, 13, 50, 67, 49, -9, 36, 43, 20, 90 };

            #region Get Min and max and indexes
            int min = arr[0];
            int max = arr[0];
            int minIndex = 0;
            int maxIndex = 0;
            for (int i = 0; i < arr.Length; i++)
            {
                if (min > arr[i])
                {
                    min = arr[i];
                    minIndex = i;
                }
                else if (max < arr[i])
                {
                    max = arr[i];
                    maxIndex = i;
                }
            }
            #endregion

            #region Change to First and Second

            int second = 0;
            int first = 0;
            if (maxIndex > minIndex)
            {
                first = minIndex;
                second = maxIndex;
            }
            else
            {
                first = maxIndex;
                second = minIndex;
            }
            #endregion


            for (int i = 0; i < first; i++)
                arr[i] = 0;
            for (int i = first + 1; i < second; i++)
                arr[i] = 1;
            for (int i = second + 1; i < arr.Length; i++)
                arr[i] = 2;

            for (int i = 0; i < arr.Length; i++)
            {
                Console.WriteLine(arr[i]);
            }

            Console.ReadLine();
        }
    }
}
